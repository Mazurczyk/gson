package gson;

public class Movie {
    String title;
    String type;
    Integer releaseDate;

    public Movie() {
    }

    public Movie(String title, String type, Integer releaseDate) {
        this.title = title;
        this.type = type;
        this.releaseDate = releaseDate;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "title='" + title + '\'' +
                ", type='" + type + '\'' +
                ", releaseDate=" + releaseDate +
                '}';
    }
}
