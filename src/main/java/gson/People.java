package gson;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class People {
    @Expose
    @SerializedName("name")
    private String fullName;
    @Expose
    private String height;
    @Expose
    @SerializedName("mass")
    private String weight;
    @Expose
    @SerializedName("hair_color")
    private String hairColor;
    @Expose
    @SerializedName("skin_color")
    private String skinColor;
    @Expose
    @SerializedName("eye_color")
    private String eyeColor;
    @Expose
    @SerializedName("birth_year")
    private String dataOfBirth;
    @Expose
    private String gender;
    @Expose
    @SerializedName("homeworld")
    private String homeWorld;
    @Expose
    private List<String> species;
    @Expose
    private List<String> vehicles;
    @Expose
    private String url;

    private String
            additionalInfo;
    private int timeStamp;

    public People() {
    }

    public People(String fullName, String height, String weight, String hairColor, String skinColor, String eyeColor, String dataOfBirth, String gender, String homeWorld, List<String> species, List<String> vehicles, String url, String additionalInfo, int timeStamp) {
        this.fullName = fullName;
        this.height = height;
        this.weight = weight;
        this.hairColor = hairColor;
        this.skinColor = skinColor;
        this.eyeColor = eyeColor;
        this.dataOfBirth = dataOfBirth;
        this.gender = gender;
        this.homeWorld = homeWorld;
        this.species = species;
        this.vehicles = vehicles;
        this.url = url;
        this.additionalInfo = additionalInfo;
        this.timeStamp = timeStamp;
    }
}
