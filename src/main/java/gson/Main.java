package gson;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Gson gson = new GsonBuilder()
                .excludeFieldsWithoutExposeAnnotation()
                .setPrettyPrinting()
                .create();
        SpaceShip spaceShip = gson.fromJson("{"+
                "\"name\": \"Death Star\",\n" +
                "\"model\": \"DS-1 Orbital Battle Station\",\n" +
                "\"manufacturer\": \"Imperial Department of Military Research, Sienar Fleet Systems\",\n" +
                "\"cost_in_credits\": \"1000000000000\",\n" +
                "\"length\": \"120000\",\n" +
                "\"max_atmosphering_speed\": \"n/a\",\n" +
                "\"crew\": \"342953\",\n" +
                "\"passengers\": \"843342\",\n" +
                "\"cargo_capacity\": \"1000000000000\",\n" +
                "\"consumables\": \"3 years\",\n" +
                "\"hyperdrive_rating\": \"4.0\",\n" +
                "\"MGLT\": \"10\",\n" +
                "\"starship_class\": \"Deep Space Mobile Battlestation\",\n" +
                "\"pilots\": [],\n" +
                "\"films\": [\n" +
                "\"https://swapi.co/api/films/1/\"\n" +
                "],\n" +
                "\"created\": \"2014-12-10T16:36:50.509000Z\",\n" +
                "\"edited\": \"2014-12-22T17:35:44.452589Z\",\n" +
                "\"url\": \"https://swapi.co/api/starships/9/\"\n" +
                "}",SpaceShip.class);
        System.out.println(spaceShip);

        People people = new People("Luke Skywalker","172","77","blond",
                "fair","blue","19BBY","male,","https://swapi.co/api/planets/1/",
                Arrays.asList("https://swapi.co/api/species/1/"),Arrays.asList("https://swapi.co/api/vehicles/14/",
                "https://swapi.co/api/vehicles/30/"),"https://swapi.co/api/people/1/","info",1);
        System.out.println(gson.toJson(people));
    }


}
