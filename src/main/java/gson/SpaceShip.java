package gson;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Arrays;

public class SpaceShip {
    @Expose
    String name;
    @Expose
    String model;
    @Expose
    String manufacturer;
    @Expose
    @SerializedName("cost_in_credits")
    String costInCredits;
    @Expose
    String length;
    @Expose
    @SerializedName("max_atmosphering_speed")
    String maxAtmospheringSpeed;
    @Expose
    String crew;
    @Expose
    String passengers;
    @Expose
    @SerializedName("cargo_capacity")
    String cargoCapacity;
    @Expose
    String consumables;
    @Expose
    @SerializedName("hyperdrive_rating")
    String hyperdriveRating;
    @Expose
    String MGLT;
    @Expose
    @SerializedName("starship_class")
    String starshipClass;
    @Expose
    String[] pilots;
    @Expose
    String[] films;
    @Expose
    String created;
    @Expose
    String edited;
    @Expose
    String url;

    @Override
    public String toString() {
        return "SpaceShip{" +
                "name='" + name + '\'' +
                ", model='" + model + '\'' +
                ", manufacturer='" + manufacturer + '\'' +
                ", costInCredits='" + costInCredits + '\'' +
                ", length='" + length + '\'' +
                ", maxAtmospheringSpeed='" + maxAtmospheringSpeed + '\'' +
                ", crew='" + crew + '\'' +
                ", passengers='" + passengers + '\'' +
                ", cargoCapacity='" + cargoCapacity + '\'' +
                ", consumables='" + consumables + '\'' +
                ", hyperdriveRating='" + hyperdriveRating + '\'' +
                ", MGLT='" + MGLT + '\'' +
                ", starshipClass='" + starshipClass + '\'' +
                ", pilots=" + Arrays.toString(pilots) +
                ", films=" + Arrays.toString(films) +
                ", created='" + created + '\'' +
                ", edited='" + edited + '\'' +
                ", url='" + url + '\'' +
                '}';
    }
}
