package http_client;

import com.google.gson.Gson;
import gson.People;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

//Na podstawie poniższego zapytania, przygotuje model danych zwierający informację o współrzędnych geograficznych
// oraz temperaturze, i pobierz dane wykorzystując klasę HttpUrlConnection:
//https://samples.openweathermap.org/data/2.5/weather?q=London,uk&appid=b6907d289e10d714a6e88b30761fae22

public class HttpRestClient {
    public static void main(String[] args) throws IOException {
        URL url = new URL("http://samples.openweathermap.org/data/2.5/weather?q=London,uk&appid=b6907d289e10d714a6e88b30761fae22");
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");
        con.addRequestProperty("User-Agent", "Mozilla/5.0");
        int status = con.getResponseCode();
        System.out.println(status);
        Gson gson = new Gson();
        BufferedReader in = new BufferedReader(
                new InputStreamReader((InputStream) con.getContent()));
        WebWeather webWeather = gson.fromJson(in, WebWeather.class);
        //String response = readResponse(con);
        //People people = gson.fromJson(response, People.class);
        System.out.println("Deserialize: ");
        System.out.println(webWeather);
        con.disconnect();
    }

    private static String readResponse(HttpURLConnection con) throws IOException {
        BufferedReader in = new BufferedReader(
                new InputStreamReader((InputStream) con.getContent()));
        String inputLine;
        StringBuffer content = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            content.append(inputLine);
        }
        in.close();
        System.out.println(content);
        return content.toString();
    }

}
