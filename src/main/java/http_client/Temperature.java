package http_client;

import com.google.gson.annotations.SerializedName;

public class Temperature {
    @SerializedName("temp")
    private float temperature;
    private int pressure;
    private int humidity;
    @SerializedName("temp_min")
    private float temperatureMin;
    @SerializedName("temp_max")
    private float temperatureMax;

}
